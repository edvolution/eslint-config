const base = require("./base.json");
const modules = require('./modules.json');;
const prettier = require("./prettier.json");

module.exports = Object.assign(base, modules, prettier);
