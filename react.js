const base = require("./base.json");
const modules = require("./modules.json");
const react = require('./react.json');
const prettier = require("./prettier.json");

module.exports = Object.assign(
  base,
  modules,
  react,
  prettier
);
