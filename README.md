# Edvolution ESLint Config

> Shared ESLint config for Edvolution

## Usage

### Installation and setup

- Install config with dependencies

  ```sh
  npx install-peerdeps --dev @edvolution/eslint-config
  ```

- Create local ESLint config

  ```sh
  echo '{
    "extends": ["@edvolution"]
  }' > .eslintrc
  ```

- Add scripts to `package.json`

  ```json
    "scripts": {
      "lint": "eslint .",
      "format": "eslint --fix ."
    }
  ```

### Alternative configurations

The default config is flexible enough for most JavaScript projects that use the Node ecosystem. That said, there are a few variations for common cases.

| Use Case | Scope                         | Extends                              |
|----------|-------------------------------|--------------------------------------|
| Default  | The baseline config           | `@edvolution`                        |
| Modules  | ES Modules i.e. import/export | `@edvolution/eslint-config/modules`  |
| React    | React and JSX support         | `@edvolution/eslint-config/react`    |
| Complete | All the configurations        | `@edvolution/eslint-config/complete` |

For example, a standard Node project but with ES Modules:

```json
{
  "extends": [
    "@edvolution",
    "@edvolution/eslint-config/modules"
  ]
}
```

## Deployment

Updates to this library can be deployed to the public namespace.

```sh
npm publish --access public
```
