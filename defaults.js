const base = require("./base.json");
const prettier = require("./prettier.json");

module.exports = Object.assign(base, prettier);
